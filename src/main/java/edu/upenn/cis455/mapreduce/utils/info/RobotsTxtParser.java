package edu.upenn.cis455.mapreduce.utils.info;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class RobotsTxtParser {
    final static Logger logger = LogManager.getLogger(RobotsTxtParser.class);
    private RobotsTxtInfo info;
    private String url;
    private boolean fileExists = true;

    public RobotsTxtParser(String url) {
        info = new RobotsTxtInfo();
        this.url = url;
    }

    public void parse() {
        String line = "";
        String currentUserAgent = null;
        logger.debug(url);
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
            while((line = in.readLine()) != null) {
                line = line.trim();
                if (line.startsWith("User-agent")) {
                    currentUserAgent = line.split(":")[1].trim();

                    info.addUserAgent(currentUserAgent);
                } else if (currentUserAgent != null) {
                    if (line.startsWith("Disallow")) {
                        String path = line.split(":")[1].trim();
                        info.addDisallowedLink(currentUserAgent, path);
                    } else if (line.startsWith("Allow")) {
                        String path = line.split(":")[1].trim();
                        info.addAllowedLink(currentUserAgent, path);
                    } else if (line.startsWith("Crawl-delay")) {
                        String delay = line.split(":")[1].trim();
                        info.addCrawlDelay(currentUserAgent, Integer.valueOf(delay));
                    } else if (line.startsWith("sitemap")) {
                        String site = line.split(":")[1].trim();
                        info.addSitemapLink(site);
                    } else {
                        //logger.debug("Line not added " + line);
                    }
                }
            }
        } catch (Exception e) {
            fileExists = false;
        }
    }

    public RobotsTxtInfo getInfo() {
        return info;
    }

    public boolean doesFileExist() {
        return fileExists;
    }

}
