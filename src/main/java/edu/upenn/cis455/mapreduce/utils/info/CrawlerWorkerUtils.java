package edu.upenn.cis455.mapreduce.utils.info;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.CrawlBolt;
import edu.upenn.cis.stormlite.bolt.DatabaseBolt;
import edu.upenn.cis.stormlite.bolt.FilterURLsBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.CrawlerSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

// class contains helper function for creating worker servers for web crawling
public class CrawlerWorkerUtils {

    static Logger logger = LogManager.getLogger(CrawlerWorkerUtils.class);

    // creates the crawler topo
    public static Topology createCrawlerTopology(Config config) {
        TopologyBuilder builder = new TopologyBuilder();

        CrawlerSpout spout = new CrawlerSpout();

        CrawlBolt crawlBolt = new CrawlBolt();
        FilterURLsBolt fillerBolt = new FilterURLsBolt();
        DatabaseBolt databaseBolt = new DatabaseBolt();


        builder.setSpout("CRAWLER_SPOUT", spout, 1);
        builder.setBolt("CRAWLER_BOLT", crawlBolt, 10).fieldsGrouping("CRAWLER_SPOUT"
                , new Fields("document"));
        builder.setBolt("FILTER_BOLT", fillerBolt, 10).fieldsGrouping("CRAWLER_BOLT",
                new Fields("document"));
        builder.setBolt("DATABASE_BOLT", databaseBolt, 1).fieldsGrouping("FILTER_BOLT",
                new Fields("document"));


        /*
        builder.setSpout("CRAWLER_SPOUT", spout, 1);
        builder.setBolt("CRAWLER_BOLT", crawlBolt, 1).firstGrouping("CRAWLER_SPOUT"
               );
        builder.setBolt("FILTER_BOLT", fillerBolt, 1).firstGrouping("CRAWLER_BOLT"
                );
        builder.setBolt("DATABASE_BOLT", databaseBolt, 1).firstGrouping("FILTER_BOLT"
                );
                */

        return builder.createTopology();
    }

    // create confic
    public static Config createCrawlerConfig(String inputDirectory, String masterPort, List<String> workers) {
        Config config = new Config();

        config.put("inputDirectory", inputDirectory);
        config.put("master", masterPort);
        config.put("job", "crawl");
        String workerString = "";
        workerString += ("[");
        for (String worker : workers) {
            workerString += (worker + ",");
        }
        workerString = workerString.substring(0, workerString.length() - 1);
        workerString += "]";
        config.put("workerList", workerString);
        logger.debug(workerString);

        return config;
    }

    // helper method sends a job to each worker registered to the master
    public static void startJobs(Topology topology, Config config, List<String> workers) {
        WorkerJob job = new WorkerJob(topology, config);
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

        try {
            int workerIndex = 0;
            for (String worker : workers) {
                config.put("workerIndex", Integer.toString(workerIndex++));
                    int responseCode = sendJobToWorker(config, worker, "POST", "/definejob",
                        mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job));
                if (responseCode != 200) {
                    throw new RuntimeException();
                }
            }

            for (String worker : workers) {
                int responseCode = sendJobToWorker(config, worker, "POST", "/runjob", "");
                if (responseCode != 200) {
                   System.out.println("Failed to send job to worker #" + worker);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
           // System.exit(0);
        }
    }

    public static void killCrawlers(List<String> workers) {
        for (String worker : workers) {
            try {
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            sendJobToWorker(null,worker, "POST", "/endcrawl", "" );
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        logger.debug("all crawlers shutdown");
    }

    public static void shutdownWorkers(List<String> workers) {
        for (String worker : workers) {
            try {
                new Thread(() -> {
                    try {
                        sendJobToWorker(null,worker, "POST", "/shutdown", "" );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        logger.debug("all crawlers shutdown");
    }

    public static int sendJobToWorker(Config config, String worker, String requestType, String path, String params)
            throws IOException {
        String strUrl = "http://" + worker + path;
        logger.debug(strUrl);
        URL url = new URL(strUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(requestType);
        connection.setDoOutput(true);

        connection.setRequestProperty("Content-Type", "application/json");
        byte[] body = params.getBytes();
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(body);
        int code = connection.getResponseCode();

        outputStream.flush();
        outputStream.close();

        return code;
    }

    public static String getWorkerStatus(List<String> workers) {
        StringBuilder res = new StringBuilder();
        for (String worker : workers) {
            try {
                String strUrl = "http://" + worker + "/status";
                logger.debug(strUrl);
                URL url = new URL(strUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(5000);
                connection.setConnectTimeout(5000);

                connection.setRequestMethod("GET");
                connection.setDoOutput(true);

                connection.setRequestProperty("Content-Type", "application/json");

                int code = connection.getResponseCode();
                if (code != 200) {
                    return "<div>Worker " + worker + "  Status was unavailable. Workers May Be Offline.</div>";
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = reader.readLine();

                res.append("<div>");
                res.append("<div>--------------</div>");
                res.append("<h3>" + line + "</h3>");
                res.append("<ul>");
                while ((line = reader.readLine()) != null) {
                    res.append("<li>" + line + "</li>");
                }
                res.append("</ul>");
                res.append("<div>--------------</div>");
                res.append("</div>");
            }
            catch (IOException e) {
                return "Status was unavailable. Workers May Be Offline.";
               // e.printStackTrace();
            }
        }
        return res.toString();
    }
}

