package edu.upenn.cis455.mapreduce.model;

import com.sleepycat.persist.model.Entity;

import java.util.ArrayList;
import java.util.List;

@Entity
public class ReduceItem  {
    String key;

    private List<String> values;

    public ReduceItem(String key) {
        this.key = key;
    }

    public void add(String val) {
        if (val == null) {
            return;
        }
        // lazy init
        if (values == null) {
            values = new ArrayList<>();
        }
        values.add(val);
    }

    public List<String> getValues() {
        if (values == null) {
            values = new ArrayList<>();
        }
        return values;
    }

    public String getKey() {
        return key;
    }
}
