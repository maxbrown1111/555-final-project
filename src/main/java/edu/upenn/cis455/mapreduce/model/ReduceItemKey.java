package edu.upenn.cis455.mapreduce.model;

import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;

public class ReduceItemKey {

    PrimaryIndex<String, ReduceItem> pIdx;

    public ReduceItemKey(EntityStore store) {
        pIdx = store.getPrimaryIndex(String.class, ReduceItem.class);
    }

    public PrimaryIndex<String, ReduceItem> getpIdx() {
        return pIdx;
    }
}
