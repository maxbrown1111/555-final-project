package edu.upenn.cis455.mapreduce.model;

import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;

public class DocumentAccessor {
    public PrimaryIndex<String, DocumentEntity> pIdx;
    EntityCursor<DocumentEntity> cursor;

    public DocumentAccessor(EntityStore store) {
        pIdx = store.getPrimaryIndex(String.class, DocumentEntity.class);
    }
}
