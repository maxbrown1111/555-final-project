package edu.upenn.cis455.mapreduce.model;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class DocumentEntity {
    @PrimaryKey
    private String url;
    private String content;
    private String lastCrawledTime;
    private String contentType;

    final static SimpleDateFormat FORMAT = new SimpleDateFormat("EEE, dd MMM YYYY HH:mm:ss z");


    List<String> outgoingURLs;

    public DocumentEntity() {

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLastCrawledTime() {
        return lastCrawledTime;
    }

    public void setLastCrawledTime() {
        Date date = new Date();
        this.lastCrawledTime = FORMAT.format(date);
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    public List<String> getOutgoingURLs() {
        if (this.outgoingURLs == null) {
            outgoingURLs = new ArrayList<>();
        }
        return outgoingURLs;
    }

    public void addOutgoingURLs(List<String> outgoingURLs) {
        if (this.outgoingURLs == null) {
            this.outgoingURLs = new ArrayList<>();
        }
        this.outgoingURLs.addAll(outgoingURLs);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocumentEntity that = (DocumentEntity) o;
        return url.equals(that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url);
    }
}
