package edu.upenn.cis455.mapreduce.storage;


import java.io.File;
import java.util.*;
import java.util.concurrent.BlockingQueue;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;
import edu.upenn.cis.stormlite.bolt.CrawlBolt;
import edu.upenn.cis455.mapreduce.model.DocumentAccessor;
import edu.upenn.cis455.mapreduce.model.DocumentEntity;
import edu.upenn.cis455.mapreduce.model.ReduceItem;
import edu.upenn.cis455.mapreduce.model.ReduceItemKey;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.print.Doc;

//source https://www.oracle.com/technetwork/database/database-technologies/berkeleydb/performing.pdf

public class DatabaseContainer  {

    Logger logger = LogManager.getLogger(DatabaseContainer.class);

    String directory;
    Environment env;
    EntityStore store;
    public DocumentAccessor documentAccessor;

    private static DatabaseContainer instance;

    boolean isOpen = false;


    // only public for master purposes
    public DatabaseContainer(String directory) {
        this.directory = directory;
        open();
    }



    // singleton for workers
    public static DatabaseContainer getInstance(String directory) {
        if (instance == null) {
            instance = new DatabaseContainer(directory);
        }
        return instance;
    }

    public static DatabaseContainer getInstance() {
        return instance;
    }

    public void open() {
        setupDirectory();
        setupEnvironment();
        setupStore();
        documentAccessor = new DocumentAccessor(store);
        isOpen = true;
    }


    private void setupEnvironment() {
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(true);
        envConfig.setAllowCreate(true);

        env = new Environment(new File(directory), envConfig);
    }

    private void setupDirectory() {
        File envDirectory = new File(directory);
        if (!envDirectory.exists()) { envDirectory.mkdir();
        } else {
            // clears regardless
            envDirectory.delete();
            envDirectory.mkdir();
        }
    }

    private void setupStore() {
        try {
            StoreConfig config = new StoreConfig();
            config.setAllowCreate(true);
            config.setTransactional(true);
            store = new EntityStore(env, "Store", config);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Environment getEnv() {
        return env;
    }

    public EntityStore getStore() {
        return store;
    }

    public void sync() {
        env.sync();
        store.sync();
    }

    public synchronized void close() {
        store.close();
        env.close();
        logger.debug("db "  + directory + " closed");
        isOpen = false;
    }

    public synchronized boolean addDocument(DocumentEntity doc) {
        doc.setLastCrawledTime();
        documentAccessor.pIdx.putNoReturn(doc);
        logger.debug("adding document " + doc.getUrl());
        sync();
        notifyAll();
        return true;
    }

    public synchronized boolean addDocuments(BlockingQueue<DocumentEntity> docs) {
        if (isOpen) {
            logger.debug("Adding docs to " + directory);
            for (DocumentEntity doc : docs) {
                doc.setLastCrawledTime();
                documentAccessor.pIdx.putNoReturn(doc);
                logger.debug("adding document " + doc.getUrl());
            }

            sync();
        } else {
            System.out.println("Database is Closed! " + docs.size() + " were not added.");
        }
        notifyAll();
        return true;
    }

    public synchronized boolean containsDocument(String url) {
        return documentAccessor.pIdx.contains(url);
    }



    public List<String> getDocumentURLs() {
        EntityCursor<String> cursor = documentAccessor.pIdx.keys();
        List<String> urls = new ArrayList<>();
        Iterator<String> iter = cursor.iterator();
        while(iter.hasNext()) {
            urls.add(iter.next());
        }
        cursor.close();
        return urls;
    }

    public int getNumDocuments() {
        EntityCursor<String> cursor = documentAccessor.pIdx.keys();
        int count = 0;
        Iterator iterator = cursor.iterator();
        while (iterator.hasNext()) {
            count++;
            iterator.next();
        }
        cursor.close();
        return count;
    }

    // merge two dbs together 1000 docs at a time to prevent OUTOFMEMORY error
    public void merge(DatabaseContainer other) {
        logger.debug("Getting docs from " + other.directory);
        EntityCursor<String> cursor = other.documentAccessor.pIdx.keys();
        Iterator<String> iter = cursor.iterator();

        // Set<DocumentEntity> batch = new LinkedHashSet<>();

        int memCounter = 0;

        List<String> keysToDelete = new LinkedList<>();
        while (iter.hasNext() && memCounter < 1000) {
            memCounter++;
            String key = iter.next();
            DocumentEntity doc = other.documentAccessor.pIdx.get(key);
            documentAccessor.pIdx.putNoReturn(doc);
            keysToDelete.add(key);
           // other.documentAccessor.pIdx.delete(key);
            //logger.debug(key);
        }
        cursor.close();

        // remove keys from other DB
        for (String key : keysToDelete) {
            other.documentAccessor.pIdx.delete(key);
        }

        // flush to system
        sync();

       // logger.debug(other.directory + " docs merged into " + directory);

    }

    public synchronized String getDocument(String url) {
        return documentAccessor.pIdx.get(url).getContent();
    }

    // cleans out files (used to clear subDBs after merging with master)
    public synchronized void clear() {
        logger.debug("clear files");
        File dir = new File(directory);
        //remove files
        for (File f : dir.listFiles()) {
            if (!f.isDirectory() && !f.getName().equals("seeds.txt") && !f.getName().equals("savedQueue.txt")) {
                logger.debug(f.getName());
                f.delete();
            }
        }

    }

    public String getPage(String url) {
        return documentAccessor.pIdx.get(url).getContent();
    }

    // helper
    public void printDocuments() {
        logger.debug(directory);
        List<String> list = getDocumentURLs();
        for (String url : list) {
            logger.debug(url);
        }
    }

    public boolean isOpen() {
        return isOpen;
    }
}
