package edu.upenn.cis455.mapreduce.worker;

import static spark.Spark.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.StatCollector;
import edu.upenn.cis.stormlite.spout.CrawlerSpout;
import edu.upenn.cis455.mapreduce.storage.DatabaseContainer;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.DistributedCluster;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.RunJobRoute;
import spark.Spark;

/**
 * Simple listener for worker creation 
 * 
 * @author zives
 *
 */
public class WorkerServer {
	static Logger log = LogManager.getLogger(WorkerServer.class);
	
    static DistributedCluster cluster = new DistributedCluster();
    static DatabaseContainer db;
    
    List<TopologyContext> contexts = new ArrayList<>();

    static Config workerConfig;
    static String storagePath;

	int myPort;
	int workerNumber;

	static boolean isCrawling = false;
	
	static List<String> topologies = new ArrayList<>();
	
	public WorkerServer(Config config) throws MalformedURLException {
		//db = DatabaseContainer.getInstance(config.get("storage"));

		storagePath = config.get("storage");
		myPort = Integer.valueOf(config.get("listeningPort"));

		workerNumber = Integer.valueOf(config.get("workerNumber"));


		//System.out.println("Creating server listener at socket " + myPort);
		workerConfig = new Config();
		port(myPort);
    	final ObjectMapper om = new ObjectMapper();
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        Spark.post("/definejob", (req, res) -> {
			isCrawling = true;

			db = DatabaseContainer.getInstance(config.get("storage"));
			if (!db.isOpen()) {
				db.open();
			}

	        	WorkerJob workerJob;
				try {
					workerJob = om.readValue(req.body(), WorkerJob.class);
		        	
		        	try {
		        		System.out.println("Processing job definition request \"" + workerJob.getConfig().get("job") +
		        				"\" on machine " + workerNumber);

						workerJob.getConfig().put("storage", storagePath);
						contexts.add(cluster.submitTopology(workerJob.getConfig().get("job"), workerJob.getConfig(), 
								workerJob.getTopology()));

						// Add a new topology
						synchronized (topologies) {
							topologies.add(workerJob.getConfig().get("job"));
						}
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		            return "Job launched";
				} catch (IOException e) {
					e.printStackTrace();
					isCrawling = false;
					// Internal server error
					res.status(500);
					return e.getMessage();
				} 
	        	
        });
        
        Spark.post("/runjob", new RunJobRoute(cluster));
        
        Spark.post("/pushdata/:stream", (req, res) -> {
				try {
					String stream = req.params(":stream");
					log.debug("Worker received: " + req.body());
					Tuple tuple = om.readValue(req.body(), Tuple.class);
					
					log.debug("Worker received: " + tuple + " for " + stream);
					
					// Find the destination stream and route to it
					StreamRouter router = cluster.getStreamRouter(stream);
					
					if (contexts.isEmpty())
						log.error("No topology context -- were we initialized??");
					
					TopologyContext ourContext = contexts.get(contexts.size() - 1);
					
					// Instrumentation for tracking progress
			    	if (!tuple.isEndOfStream())
			    		ourContext.incSendOutputs(router.getKey(tuple.getValues()));

			    	// maybe wrong executeor
			    	if (tuple.isEndOfStream()) {
			    		router.executeEndOfStreamLocally(ourContext, tuple.getSourceExecutor());
					} else {
			    		router.executeLocally(tuple, ourContext,
								tuple.getSourceExecutor());
					}

			    	// TODO: handle tuple vs end of stream for our *local nodes only*
			    	// Please look at StreamRouter and its methods (execute, executeEndOfStream, executeLocally, executeEndOfStreamLocally)
					
					return "OK";
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
					res.status(500);
					return e.getMessage();
				}
				
        });

        Spark.post("/endcrawl", (request, response) -> {
			isCrawling = false;
        	System.out.println("Shutting down Crawler");
        	shutdownJobs();
        	// wait for db to close
        	response.status(200);

        	return "<html>Crawl ended</html>";
		});

		Spark.post("/shutdown", (request, response) -> {
			System.out.println("Shutting down server");

			new Thread(() -> {
				// time to return response
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				shutdownServer();
			}).start();

			response.status(200);

			return "<html>Server Shutdown</html>";
		});

		Spark.get("/status", (request, response) -> {
			StatCollector instance = StatCollector.getInstance(workerNumber);
			StringBuilder out = new StringBuilder();
			out.append("Worker #" + workerNumber +  " Status:\r\n");
			String crawlStatus = isCrawling ? "Active" : "Inactive";
			out.append("Crawling: " + crawlStatus + "\r\n");
			out.append(instance.getNumberCrawled() + "\r\n");
			out.append(instance.getNumberIndexed() + "\r\n");

			response.status(200);
			response.body(out.toString());
			return out.toString();
		});
	}
	
	public static void createWorker(Config config) {
		/*
		if (!config.containsKey("workerList"))
			throw new RuntimeException("Worker spout doesn't have list of worker IP addresses/ports");

		if (!config.containsKey("workerIndex"))
			throw new RuntimeException("Worker spout doesn't know its worker ID");
		else {
		*/

			//String[] addresses = WorkerHelper.getWorkers(config);
			//String myAddress = addresses[Integer.valueOf(config.get("workerIndex"))];
			//log.debug("Initializing worker " + myAddress);

			URL url;
			try {
				//url = new URL(myAddress);

				new WorkerServer(config);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		// }
	}

	public static void shutdownJobs() {
		synchronized(topologies) {
			for (String topo: topologies)
				cluster.killTopology(topo);
		}

		// incase topologies were not initialize
		try {
			CrawlerSpout.writeQueueToFile(storagePath);
		} catch (NullPointerException e) {

		}

		cluster.shutdown();

		synchronized (cluster) {
			db.close();
		}
	}

	public static void shutdownServer() {
		shutdownJobs();
		System.exit(0);
	}

	/**
	 * Simple launch for worker server.  Note that you may want to change / replace
	 * most of this.
	 * 
	 * @param args
	 * @throws MalformedURLException
	 */
	public static void main(String args[]) throws MalformedURLException {
		// org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
		if (args.length < 1) {
			System.out.println("Usage: WorkerServer [port number]");
			System.exit(1);
		}
		
		int myPort = Integer.valueOf(args[1]);
		
		System.out.println("Worker node startup, on ip " + args[0] + " on port " + myPort);

		Config config = new Config();

		config.put("workerIp", args[0]);
		config.put("listeningPort", args[1]);
		config.put("storage", "store/db" + args[2]);

		// DIFFERENT FROM WORKER INDEX
		config.put("workerNumber", args[2]);
		config.put("masterPort", args[3]);

		WorkerServer.createWorker(config);

	}

	public static Config getWorkerConfig() {
		return workerConfig;
	}
}
