package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.IndexStorage.TfIdfDatabaseContainer;
import edu.upenn.cis.stormlite.*;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.WordFileSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis455.mapreduce.model.DocumentEntity;
import edu.upenn.cis455.mapreduce.storage.DatabaseContainer;
import edu.upenn.cis455.mapreduce.utils.info.CrawlerWorkerUtils;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;
import org.apache.logging.log4j.Level;

import java.io.*;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


public class MasterServer {

  static final long serialVersionUID = 455555001;
  static final int myPort = 8080;

  static List<String> workers = null;
  static List<String> childDatabases = null;
  static DatabaseContainer masterDB;

  static long crawlBegin = 0;
  static long crawlEnd = 0;

  static String ipAddress = "";

  static TfIdfDatabaseContainer searchDb = TfIdfDatabaseContainer.getInstance("store/indexDB");


  public static void registerStatsRoute() {
      // to see the DB subdirectory counts and master count
      get("/stats", ((request, response) -> {
          try {

              DatabaseContainer m = new DatabaseContainer("store/master");

              int newDoumentsCrawled = 0;
              for (String dbname : childDatabases) {
                  DatabaseContainer db = new DatabaseContainer(dbname);
                  System.out.println(dbname + " has " + db.getNumDocuments() + " documents");
                  newDoumentsCrawled += db.getNumDocuments();
                  db.close();
              }
              System.out.println("Master has " + m.getNumDocuments());
              double secondsElapsed = (crawlEnd - crawlBegin) / 1000.0;
              double docsPerSecond = (newDoumentsCrawled / secondsElapsed);
              System.out.println("Crawler has crawled " + newDoumentsCrawled + " in " + secondsElapsed +
                      "seconds; rate = " + docsPerSecond + " docs per second");

              m.close();
          } catch (Exception e) {
              e.printStackTrace();
          }

          response.status(200);
          return "";
      }));
  }

  public static void registerStatusPage() {
		get("/status", (request, response) -> {
            response.type("text/html");

            StringBuilder sb = new StringBuilder();
            sb.append("<html><head><title>Master</title></head>\n");
            sb.append("<body><h1>Master Status</h1>");
            sb.append("<div>");
            sb.append(CrawlerWorkerUtils.getWorkerStatus(workers));
            sb.append("</div>");
            sb.append("</html>");
            return (sb.toString());
		});
		
  }


    private static void registerQueryRoute() {
      post("/results", ((request, response) -> {
          String queryString = request.queryParams("query");
          System.out.println("Sending results for search query: " + queryString);

          String[] results = searchDb.search(queryString);

          StringBuilder sb = new StringBuilder();
          for (String result : results) {
              sb.append(result+"\r\n");
          }
          response.body(sb.toString());
          response.status(200);
        //  response.body("success");
          return sb.toString();
      }));

      get("/workerstatus", ((request, response) -> {
         StringBuilder out = new StringBuilder();
         String res = CrawlerWorkerUtils.getWorkerStatus(workers);
         System.out.println(res);

         response.status(200);
         return res;
      }));
    }



    private static void registerCrawlerStart() {
        post("/crawl", (request, response) -> {

            String inputDirectory = request.params("inputDirectory");
            String masterPort = Integer.toString(request.port());
            Config config = CrawlerWorkerUtils.createCrawlerConfig(inputDirectory, masterPort, workers);
            Topology topology = CrawlerWorkerUtils.createCrawlerTopology(config);
            CrawlerWorkerUtils.startJobs(topology, config, workers);
            crawlBegin = new Date().getTime();

            System.out.println("Crawler Jobs have been sent out at time: " +
                    StatCollector.dateFormat.format(new Date()));

            return "<html>Crawls have been successfully sent!</html>";
        });

        post("/endcrawl", ((request, response) -> {
            CrawlerWorkerUtils.killCrawlers(workers);
            crawlEnd = new Date().getTime();
            return "<html>End crawls have been successfully sent!</html>";
        }));

        get("/shutdownworkers", ((request, response) -> {
            CrawlerWorkerUtils.shutdownWorkers(workers);
            response.status(200);
            return "<html>Telling workers to shutdown</html>";
        }));
    }

    private static void registerMergeDBs() {
      get("/mergeDBs", ((request, response) -> {
          System.out.println("Merging Worker Databases into Master Database");

          // merge
          MergeThread thread = new MergeThread(childDatabases, masterDB);
          thread.start();

          response.status(200);
          return "<html>Merging DBs</html>";
      }));
    }

    private static void registerShutdownRoute() {
      get("/shutdown", ((request, response) -> {
          new Thread() {
              @Override
              public void run() {
                  //wait to send respons back
                  try {
                      Thread.sleep(5000);
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  }
                  shutdown();
              }
          }.start();

          response.status(200);
          return "<html>Shutting down</html>";
      }));
    }



    private static void addWorkers(int numWorkers) {
        workers = new ArrayList<>();
        childDatabases = new ArrayList<>();

        for (int i = 0; i < numWorkers; i++) {
            workers.add(ipAddress + ":808" + (i + 1));
            childDatabases.add("store/db" + (i+1));
        }
    }

    private static void shutdown() {
      System.out.println("Shutting down master server");
        if (masterDB != null) {
            masterDB.close();
        }

        // shutdown spark server
        stop();

        // end java instance;
        System.exit(0);

    }
    private static class MergeThread extends Thread {
      List<String> childDatabases;
      DatabaseContainer masterDB;

      public MergeThread(List<String> childDatabases, DatabaseContainer masterDB) {
          this.childDatabases = childDatabases;
          this.masterDB = masterDB;
      }

        @Override
        public void run() {
          mergeDBs();
        }

        // combines worker DBs into master db for index building
        private void mergeDBs() {
            for (String dbPath : childDatabases) {
                masterDB = new DatabaseContainer("store/master");

                DatabaseContainer db = new DatabaseContainer(dbPath);

                int numberOfDocs = db.getNumDocuments();
                // copy over docs 1000 at a time
                for (int i = 0; i <numberOfDocs; i+=1000) {
                    masterDB.merge(db);
                }

                // close dbs for heap issues
                db.close();
                masterDB.close();

                // empty out old docs to save disk space
                db.clear();
                System.out.println("Database at " + dbPath + " has been merged into /master and cleared");
            }

            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            masterDB = new DatabaseContainer("store/master");
           // masterDB.printDocuments();
            masterDB.close();
            System.out.println("Merge Completed");
        }
    }

    /**
   * The mainline for launching a MapReduce Master.  This should
   * handle at least the status and workerstatus routes, and optionally
   * initialize a worker as well.
   * 
   * @param args
   */
    public static void main(String[] args) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
		System.out.println("MasterServer starting up, running on ip " + args[0] + ", at port " + args[1] + " with "
                + args[2] + " workers at time " + StatCollector.dateFormat.format(new Date()));

		int numWorkers = Integer.parseInt(args[2]);

		ipAddress = args[0];
        ipAddress(args[0]);
        port(Integer.parseInt(args[1]));

		addWorkers(numWorkers);
		registerStatusPage();
		registerCrawlerStart();
		registerStatsRoute();
        awaitInitialization();
        //registerHomePage();
        registerQueryRoute();
        registerMergeDBs();
        registerShutdownRoute();



        // tried creating workers from master
        /*
        *         for (int i = 0; i < numWorkers; i++) {
            Config config = new Config();
            config.put("workerIp", "localhost");
            config.put("listeningPort", (808 + (i + 1)) + "");
            config.put("storage", "store/db" + (i + 1));

            // DIFFERENT FROM WORKER INDEX
            config.put("workerNumber", (i + 1) + "");
            config.put("masterPort", args[1]);

            WorkerServer.createWorker(config);
        }*/




	}

	private static List<String> dummyResults() {
        List<String> results = new LinkedList<>();

        results.add("https://en.wikipedia.org/wiki/Social_research");
        results.add("https://en.wikipedia.org/wiki/Place_name");
        results.add("https://en.wikipedia.org/wiki/The_Justice_of_Trajan_and_Herkinbald");
        results.add("https://en.wikipedia.org/wiki/Gouda,_South_Holland");
        results.add("https://en.wikipedia.org/wiki/Waddinxveen");
        results.add("https://en.wikipedia.org/wiki/Alkmaar");
        results.add("https://en.wikipedia.org/wiki/Delft");
        results.add("https://en.wikipedia.org/wiki/Districts_of_Antwerp");
        results.add("https://en.wikipedia.org/wiki/Portrait_of_a_Man_(Self_Portrait%3F)");
        results.add("https://en.wikipedia.org/wiki/National_Gallery");
        results.add( "https://en.wiktionary.org/wiki/Mozambique");

        return results;

    }



}
  
