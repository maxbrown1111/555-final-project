package edu.upenn;

import edu.upenn.cis.IndexStorage.TfIdfDatabaseContainer;
import org.apache.logging.log4j.Level;

import java.awt.*;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

import static spark.Spark.*;

public class SearchServer {
    String ip;
    String port;
    String resultsIP;
    String resultsPort;

  //  TfIdfDatabaseContainer db = TfIdfDatabaseContainer.getInstance("store/indexDB");


    public SearchServer(String ip, String port, String resultsIP, String resultsPort) {
        this.ip = ip;
        this.port = port;
        this.resultsIP = resultsIP;
        this.resultsPort = resultsPort;
     //   this.resultsServerAddress = resultsServerAddress;
    }

    private void start() {
        ipAddress(ip);
        port(Integer.valueOf(port));
        init();

        // define routes
        get("/", ((request, response) -> {
            System.out.println("home page loaded");
            // get html home page
            StringBuilder contentBuilder = new StringBuilder();
            try {
                BufferedReader in = new BufferedReader(new FileReader("view.html"));
                String str;
                while ((str = in.readLine()) != null) {
                    contentBuilder.append(str);
                }
                in.close();
            } catch (Exception e) {
            }
            byte[] content = contentBuilder.toString().getBytes();
            // System.out.println(contentBuilder.toString());
            response.status(200);
            response.body(contentBuilder.toString());
            return content;
        }));

        get("/results", ((request, response) -> {

            String queryString = request.queryParams("query");
            System.out.println(queryString + " was searched");


                List<String> results = getResults(queryString);


                StringBuilder out = new StringBuilder();
                out.append("<div class=\"result-container\">");
                for (String result : results) {
                   // System.out.println(result);
                    out.append("<div class=\"result-item\">");
                    out.append("<a href=\"");
                    out.append(result);
                    out.append("\">");
                    out.append(result);
                    out.append("</a>");
                    out.append("</div>");
                }
                out.append("</div>");

                System.out.println(out.toString());

                response.status(200);
                response.body(out.toString());
                return out.toString();
        }));
    }

    // https://examples.javacodegeeks.com/core-java/net/socket/send-http-post-request-with-socket/
    private List<String> getResults(String queryString) {
        List<String> results = new LinkedList<>();
        try {

            String params = URLEncoder.encode("query", "UTF-8")
                    + "=" + URLEncoder.encode(queryString, "UTF-8");

            Socket socket = new Socket(resultsIP, Integer.valueOf(resultsPort));
            PrintWriter pw = new PrintWriter(socket.getOutputStream());

            System.out.println(socket.getInetAddress());

            pw.write("POST " + "/results" + " HTTP/1.1\r\n");
            pw.write("Host: " +resultsIP + "\r\n");
            pw.write("Content-Length: "+params.length()+"\r\n");
            pw.write("Content-Type: application/x-www-form-urlencoded\r\n");
            pw.write("\r\n");
            pw.write(params);
            pw.write("\r\n");
            pw.flush();

            BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line;

            while ((line = rd.readLine()) != null) {
                System.out.println(line + " from response");
                if (line.trim().startsWith("http")) {
                    results.add(line.trim());
                }
                if (results.size() > 9) {
                    break;
                }
            }
            System.out.println("outside while");

            rd.close();
            pw.close();
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        System.out.println("returning results");
        return results;

    }


    public static void main(String args[]) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        String ip = args[0];
        String port = args[1];
        String resultsIP = args[2];
        String resultsPort = args[3];

        SearchServer server = new SearchServer(ip, port, resultsIP, resultsPort );
        System.out.println("Starting Server");
        server.start();
    }
}
