package edu.upenn.cis.IndexStorage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@Entity
public class TfIdfEntity {
    @PrimaryKey
    private String word;
    private HashMap<String, Double> tf;
    private HashMap<String, Double> t_tf;
    private int doc_count;

    // max top caching
    private int cache_n = 15;
    private Set<String> top_results;
    private Set<String> t_top_results;

    public TfIdfEntity() {
        tf = new HashMap<String, Double>();
    }

    public TfIdfEntity(String word) {
        this.word = word;
        tf = new HashMap<String, Double>();
        t_tf = new HashMap<String, Double>();
        top_results = new HashSet<String>();
        t_top_results = new HashSet<String>();
    }

    public String get_word() {
        return word;
    }

    public void set_word(String word) {
        this.word = word;
    }

    public Double get_tf(String doc) {
        if (!tf.containsKey(doc)) return 0.0;
        return tf.get(doc);
    }

    public Double get_ttf(String doc) {
        if (!t_tf.containsKey(doc)) return 0.0;
        return t_tf.get(doc);
    }

    public void set_ttf(String doc, Double val) {
        t_tf.put(doc, val);
        Double min = val;
        String min_doc = doc;
        t_top_results.add(doc);
        if (t_top_results.size() > cache_n) {
            for (String d : t_top_results) {
                if (t_tf.get(d) < min) {
                    min_doc = d;
                    min = val;
                }
            }
            t_top_results.remove(min_doc);
        }
    }

    public void set_tf(String doc, Double val) {
        if (!tf.containsKey(doc)) doc_count++;
        tf.put(doc, val);
        Double min = val;
        String min_doc = doc;
        top_results.add(doc);
        if (top_results.size() > cache_n) {
            for (String d : top_results) {
                if (tf.get(d) < min) {
                    min_doc = d;
                    min = val;
                }
            }
            top_results.remove(min_doc);
        }
    }

    public Double get_idf(int corpus_size) {
        if (tf.size() == 0 || corpus_size == 0) return 0.0;
        return Math.log((double)corpus_size/doc_count)/Math.log(2);
    }

    public Double get_tfidf(String doc, int corpus_size) {
        return get_tf(doc)*get_idf(corpus_size)+get_ttf(doc)*0.5;
    }

    public Set<String> get_top_docs(int size) {
        HashSet<String> res = new HashSet<String>(top_results);
        for (String s : t_top_results)
            res.add(s);
        return res;
    }
}