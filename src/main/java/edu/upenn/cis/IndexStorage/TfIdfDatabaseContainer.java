package edu.upenn.cis.IndexStorage;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;

import java.io.File;
import java.util.*;

public class TfIdfDatabaseContainer {

    private static int PAGE_LENGTH = 10;
    private static Double TITLE_RANKING_WEIGHT = 0.5;

    static String directory;
    static Environment env;
    static EntityStore store;
    public TfIdfAccessor tfIdfAccessor;
    public EntityCursor<TfIdfEntity> cursor;

    private HashSet<String> banned;

    private static TfIdfDatabaseContainer instance;


    private TfIdfDatabaseContainer(String directory) {
        this.directory = directory;
        setupDirectory();
        setupEnvironment();
        setupStore();
        tfIdfAccessor = new TfIdfAccessor(store);

        banned = new HashSet<String>();
    }

    public static TfIdfDatabaseContainer getInstance(String directory) {
        if (instance == null) {
            instance = new TfIdfDatabaseContainer(directory);
        }
        return instance;
    }

    public static TfIdfDatabaseContainer getInstance() {
        return instance;
    }

    private void setupEnvironment() {
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(true);
        envConfig.setAllowCreate(true);

        env = new Environment(new File(directory), envConfig);
    }

    private void setupDirectory() {
        File envDirectory = new File(directory);
        if (!envDirectory.exists()) { envDirectory.mkdir();
        } else {
            // clears regardless
            envDirectory.delete();
            envDirectory.mkdir();
        }
    }

    private void setupStore() {
        try {
            StoreConfig config = new StoreConfig();
            config.setAllowCreate(true);
            config.setTransactional(true);
            store = new EntityStore(env, "Store", config);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Environment getEnv() {
        return env;
    }

    public EntityStore getStore() {
        return store;
    }

    public void sync() {
        env.sync();
        store.sync();
    }

    public void close() {
        store.close();
        env.close();
    }

    public synchronized boolean addTfIdf(TfIdfEntity tf) {
        tfIdfAccessor.pIdx.putNoReturn(tf);
        sync();
        notifyAll();
        return true;
    }

    public List<String> getWords() {
        EntityCursor<String> cursor = tfIdfAccessor.pIdx.keys();
        List<String> words = new ArrayList<>();
        Iterator<String> iter = cursor.iterator();
        while(iter.hasNext()) {
            words.add(iter.next());
        }
        cursor.close();
        return words;
    }

    public void printWords() {
        List<String> list = getWords();
        for (String word : list) {
            System.out.println(word);
        }
    }

    // SEARCH FUNCTIONS
    private Set<String> top_docs(String term) {
        if (tfIdfAccessor.pIdx.contains(term))
            return tfIdfAccessor.pIdx.get(term).get_top_docs(PAGE_LENGTH);
        return new HashSet<String>();
    }

    private Double tf(String term, String doc) {
        if (tfIdfAccessor.pIdx.contains(term))
            return tfIdfAccessor.pIdx.get(term).get_tf(doc);
        return 0.0;
    }

    private Double idf(String term, int corpus_size) {
        if (tfIdfAccessor.pIdx.contains(term))
            return tfIdfAccessor.pIdx.get(term).get_idf(corpus_size);
        return 0.0;
    }

    private Double ttf(String term, String doc) {
        if (tfIdfAccessor.pIdx.contains(term))
            return tfIdfAccessor.pIdx.get(term).get_ttf(doc);
        return 0.0;
    }

    private Double tf_idf(String term, String doc, int corpus_size) {
        return (tf(term, doc)+ ttf(term, doc)*TITLE_RANKING_WEIGHT)*idf(term, corpus_size);
    }

    public String[] search(String str) {
        int corpus_size = Math.toIntExact(tfIdfAccessor.pIdx.count());
        HashMap<String, Integer> query = query_parse(str.toLowerCase());
        HashMap<String, Double> docs = new HashMap<String, Double>();
        for (String word : query.keySet())
            for (String doc : top_docs(word))
                docs.put(doc, 0.0);
        for (String doc : docs.keySet())
            for (String q : query.keySet())
                docs.put(doc, docs.get(doc) + tf_idf(q, doc, corpus_size));
        // SELECTING TOP RESULTS -------------------------
        int min_score = 0;
        String[] results = new String[PAGE_LENGTH];
        Double insert_score, new_score;
        String insert_name, new_name;
        for (String doc : docs.keySet()) {
            //System.out.println("DOCUMENT "+doc+" -> "+docs.get(doc));
            insert_score = docs.get(doc);
            insert_name = doc;
            for (int i = 0; i < PAGE_LENGTH; i++) {
                if (docs.get(results[i])==null) {
                    results[i] = insert_name;
                    break;
                } else if (docs.get(results[i]) <= insert_score) {
                    new_score = docs.get(results[i]);
                    new_name = results[i];
                    results[i] = insert_name;
                    insert_score = new_score;
                    insert_name = new_name;
                }
            }
        }
        return results;
    }

    public HashMap<String, Integer> query_parse(String str) {
        String[] words = str.split(" ");
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for (String w : words) {
            if (!banned.contains(w)) {
                if (map.containsKey(w)) {
                    map.put(w, map.get(w) + 1);
                } else {
                    map.put(w, 1);
                }
            }
        }
        return map;
    }


}