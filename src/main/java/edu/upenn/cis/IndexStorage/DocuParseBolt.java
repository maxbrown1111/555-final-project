package edu.upenn.cis.IndexStorage;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.mapreduce.model.DocumentEntity;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DocuParseBolt implements IRichBolt {

    static final String executorID = UUID.randomUUID().toString();

    private Map<String, String> stormConf;
    private OutputCollector collector;
    private TopologyContext context;

    private int total_docs;

    private Fields schema = new Fields("url", "html");
    private Fields schema_out = new Fields("word", "url","tf","is_title_tf"); // tf_type true for Title

    @Override
    public void cleanup() {

    }

    @Override
    public boolean execute(Tuple input) {
        total_docs++;

        // limit number of non-english words from index
        String url = (String) input.getObjectByField("url");
        if (url.contains("wikipedia") && !url.contains("en")) {
            return false;
        }


        String html = (String) input.getObjectByField("html");
        if (html != null) if (html.length() > 0) {
            if (html.length() > 200000) {
                html = html.substring(0, 200000);
            }

            Document doc = Jsoup.parse(html);
            DocuParse dp = new DocuParse();
            String raw = dp.clean(doc.text());
            String raw_title = dp.clean(doc.title());
            HashMap<String, Double> freq = dp.calc_freqs(raw);
            HashMap<String, Double> title_freq = dp.calc_freqs(raw_title);
            for (String w : freq.keySet()) {
                this.collector.emit(new Values(w, url, freq.get(w), false), executorID);
            }
            for (String w : title_freq.keySet()) {
                this.collector.emit(new Values(w, url, title_freq.get(w), true), executorID);
            }
        }
        return true;
    }

    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.stormConf = stormConf;
        this.collector = collector;
        this.context = context;
        total_docs = 0;
    }

    @Override
    public void setRouter(StreamRouter router) {
        this.collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return schema_out;
    }

    @Override
    public String getExecutorId() {
        return executorID;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(schema_out);
    }
}
