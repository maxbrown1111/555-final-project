package edu.upenn.cis.IndexStorage;

import java.time.Instant;

public class LoadingGUI {

    public String name;
    public int total;
    public int interval;
    public long last_epoch;
    private int count;

    public LoadingGUI(String name, int total, int time) {
        this.name = name;
        this.total = total;
        this.interval = time;
        last_epoch = 0;
        count = 0;
    }

    public void load() {
        count++;
        long time = Instant.now().toEpochMilli();
        if (time - last_epoch > interval || count == total) {
            System.out.println("----- PACE LOG -----   "+name+": "+(((int)(count*1000000.0/total))/10000.0)+"% TIMESTAMP:"+(new java.util.Date()));
            last_epoch = time;
        }
    }
}