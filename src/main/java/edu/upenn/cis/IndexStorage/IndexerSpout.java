package edu.upenn.cis.IndexStorage;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.spout.IRichSpout;
import edu.upenn.cis.stormlite.spout.SpoutOutputCollector;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.mapreduce.storage.DatabaseContainer;
import org.apache.commons.lang3.StringEscapeUtils;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class IndexerSpout implements IRichSpout {

    private static String SCRAPE_LOCATION = "store/master";

    String executorId = UUID.randomUUID().toString();
    private Map<String, String> config;
    private TopologyContext topo;
    private SpoutOutputCollector collector;
    private DatabaseContainer db;
    private List<String> urls;
    private boolean db_open = false;
    private boolean db_eject = true;


    private Fields schema = new Fields("url", "html");
    private LoadingGUI loader;

    @Override
    public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
        this.config = config;
        this.topo = topo;
        this.collector = collector;
        if (!db_open) {
            db = DatabaseContainer.getInstance(SCRAPE_LOCATION);
            urls = db.getDocumentURLs();
            loader = new LoadingGUI("Java Spout", urls.size(), 3000);
            db_open = true;

        }
    }

    @Override
    public void close() {
        db_open = false;
        while (!db_eject) {}
        db.close();
    }

    @Override
    public synchronized boolean nextTuple() {
        loader.load();
        if (urls.size() > 0 && db_open) {
            db_eject = false;
            String url = urls.get(0);
            urls.remove(0);
            String page_data = StringEscapeUtils.escapeJava(db.getPage(url));
            this.collector.emit(new Values(url, page_data), executorId);//db.getDocument(url)));
            db_eject = true;
            return true;
        } else {
            return false;
        }
    }

    public void setRouter(StreamRouter router) {
        collector.setRouter(router);
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("url", "html"));
    }


}