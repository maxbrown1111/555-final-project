package edu.upenn.cis.IndexStorage;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static spark.Spark.port;

public class DocuParse {

    private HashSet<String> banned;

    public DocuParse() {
        banned = new HashSet<String>();
        add_banned();

    }

    private void add_banned() {
        String[] b = new String[]{""," ", "  ","don't","a","t","i", "about", "an", "are", "as", "at", "be", "by", "com", "de", "en", "for", "from", "how",
                "in", "is", "it", "la", "of", "on", "or", "that", "the", "this", "to", "was", "what", "when", "where",
                "who", "will", "with", "and", "www"};
        for (String word : b)
            banned.add(word);
    }

    public HashMap<String, Double> parseDoc(String html) {
        //String raw = clean(strip_whitespace(strip_tags(html))).toLowerCase();
        String raw = get_raw_text(html);
        return calc_freqs(raw);
    }

    public HashMap<String, Double>calc_freqs(String raw) {
        HashMap<String, Double> term_freq = new HashMap<String, Double>() ;
        String[] words = raw.split(" ");
        double unit = 1.0/words.length;
        if (unit < 0)
            System.out.println("Doc Unit is :"+unit);
        for (String w : words) {
            if (term_freq.containsKey(w)) {
                term_freq.put(w, term_freq.get(w)+unit);
            } else {
                if (!banned.contains(w))
                    term_freq.put(w, unit);
            }
        }
        return term_freq;
    }

    public String get_raw_text(String html) {
        if (html == null) return "";

        // Document doc = Jsoup.parse(html);
        if (html.length() != 2000000) {
            html = html.substring(0, 2000000);
        }
        return html;
    }

    private String parse_title(String html) {
        return "";
    }

    private String strip_tags(String html) {
        String acc = "";
        boolean in_tag = false;
        boolean last_tag_space = true;
        int len = html.length();
        for (int i = 0; i < html.length(); i++) {
            char c = html.charAt(i);
            if (c == '<') in_tag = true;
            if (!in_tag) {
                acc += c;
            }
            if (c == '>' && in_tag) {
                in_tag = false;
                acc += ' ';
            }
        }
        return acc;
    }

    private String strip_whitespace(String input) {
        String acc = "";
        boolean last_space = true;
        int len = input.length();
        for (int i = 0; i < len; i++) {
            char c = input.charAt(i);
            if (c == ' ') {
                if (!last_space) {
                    last_space = true;
                    acc += ' ';
                }
            } else {
                last_space = false;
                acc += c;
            }
        }
        return acc;
    }

    // removes all bad characters (such as periods)
    public String clean(String raw) {
        StringBuilder sb = new StringBuilder();
        int len = raw.length();
        for (int i = 0; i < len; i++) {
            char c = raw.charAt(i);
            if (c >= 'a' && c <= 'z') sb.append(c);
            else if (c >= 'A' && c <= 'Z') sb.append(c + 32);
            else if (c == ' ' || c == '.' || c == ',') sb.append(' ');
            else sb.append(' ');
        }
        return sb.toString();
    }
}
