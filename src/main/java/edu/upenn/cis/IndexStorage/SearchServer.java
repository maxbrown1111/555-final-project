package edu.upenn.cis.IndexStorage;

import edu.upenn.cis455.mapreduce.storage.DatabaseContainer;

import java.time.Instant;

import static spark.Spark.*;

public class SearchServer {

    private static int num_search_dbs = 1;
    private static String INDEXED_DATA_LOCATION = "store/indexDB";
    private String scrape_db;
    private TfIdfDatabaseContainer search_db;
    private int my_port = 8000;

    public SearchServer(int port) {
        my_port = port;
        port(my_port);
    }

    public void launch() {
        System.out.println("Opening Database");
        search_db = TfIdfDatabaseContainer.getInstance(INDEXED_DATA_LOCATION);
        registerHomePage();
        registerCloseDBPage();
        registerSearchQuery();
        System.out.println("Server Started");
        awaitInitialization();
    }

    public void close() {
        search_db.close();
    }

    public void registerCloseDBPage() {
        get("/close", (request, response) -> {
            response.type("text/html");
            this.close();
            return ("<html><head><title>Master</title></head>\n" +
                    "<body>Hi, closing down search engine</body></html>");
        });
    }

    public void registerHomePage() {
        get("/home", (request, response) -> {
            response.type("text/html");

            return ("<html><head><title>Master</title></head>\n" +
                    "<body>Hi, I am the master search server!</body></html>");
        });
    }

    public void registerSearchQuery() {
        get("/", (request, response) -> {
            response.type("text/html");
            String query = request.queryParams("q");
            if (query == null) {
                query = "";
            } else {
                query = query.toLowerCase();
            }
            String[] results = search_db.search(query);
            String resp = "<html><head><title>Search Results</title></head><body>";
            resp+="<form method='GET'><input type='text' name='q' id='q' value ='"+query+"' autofocus><input type='submit'></form>";
            for (String r : results)
                if (r != null)
                    resp += "<a href='"+r+"'>"+r+"</a><br><br>";
            resp += "</body></html>";
            return resp;
        });
    }

    public static void main(String[] args) {
        /*
        System.out.println("Opening Database...");
        DatabaseContainer db = DatabaseContainer.getInstance("store/indexDB");
        System.out.println("Printing items...");
        long now = Instant.now().toEpochMilli();
        System.out.println("NUM DOCUMENTS: "+db.getNumDocuments());
        long now2 = Instant.now().toEpochMilli();
        System.out.println("Compute time: "+(now2-now));
        System.out.println("Done printing");
        db.close();
        System.out.println("Database Closed");
         */
        if (args.length == 0) {
            SearchServer server = new SearchServer(8000);
            server.launch();
        } else {
            SearchServer server = new SearchServer(Integer.parseInt(args[0]));
            server.launch();
        }
    }
}