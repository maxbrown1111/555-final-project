package edu.upenn.cis.IndexStorage;


import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;

public class TfIdfAccessor {
    public PrimaryIndex<String, TfIdfEntity> pIdx;
    EntityCursor<TfIdfEntity> cursor;

    public TfIdfAccessor(EntityStore store) {
        pIdx = store.getPrimaryIndex(String.class, TfIdfEntity.class);
    }
}