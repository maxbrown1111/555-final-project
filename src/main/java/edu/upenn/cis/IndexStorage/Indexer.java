package edu.upenn.cis.IndexStorage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.tuple.Fields;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static spark.Spark.get;
import static spark.Spark.port;

/**
 * Simple word counter test case, largely derived from
 * https://github.com/apache/storm/tree/master/examples/storm-mongodb-examples
 *
 */
/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Indexer {

    private static int NUM_PRROCESSING_BOLTS = 2;
    private static int NUM_COLLECTOR_BOLTS = 8;
    private static int MAX_RUN_TIME = 172800    ;

    private static int EMERGENCY_SHUTDOWN_PORT = 8030;
    private static int EMERGENCY_SHUTDOWN = (int) Math.floor(Math.random()*1000000000);

    private static LocalCluster cluster;
    private static boolean cluster_alive = true;

    private static final String DOCUMENT_SPOUT = "DOCUMENT_SPOUT";
    private static final String PROCESS_BOLT = "PROCESS_BOLT";
    private static final String ACC_BOLT = "ACC_BOLT";

    public static void shutdown() {
        if (cluster_alive) {
            cluster.killTopology("Indexer");
            cluster.shutdown();
            System.exit(0);
        }
    }

    public static void main(String[] args) throws Exception {
        if(args.length > 0) MAX_RUN_TIME = Integer.parseInt(args[0]);
        if(args.length > 1) NUM_PRROCESSING_BOLTS = Integer.parseInt(args[1]);
        if(args.length > 2) {
            NUM_COLLECTOR_BOLTS = Integer.parseInt(args[2]);
        } else {
            NUM_COLLECTOR_BOLTS = 4*NUM_PRROCESSING_BOLTS;
        }

        // EMERGENCY SHUTDOWN CODE
        port(EMERGENCY_SHUTDOWN_PORT);
        get("/", (request, response) -> {
                    response.type("text/html");
                    String query = request.queryParams("q");
                    if (query != null) if (Integer.parseInt(query) == EMERGENCY_SHUTDOWN) {
                        shutdown();
                        return "Initializing Shutdown";
                    }
                    return "Begin shutdown sequence? <a href='?q="+EMERGENCY_SHUTDOWN+"'>Yes</a><br><br>Note: The following page will NOT load, however clicking this button will initilize the shutdown sequence";

        });
        // END EMERGENCY CODE


        Config config = new Config();

        IndexerSpout spout = new IndexerSpout();
        DocuParseBolt bolt = new DocuParseBolt();
        WordCollectorBolt acc_bolt = new WordCollectorBolt();

        // wordSpout ==> countBolt ==> MongoInsertBolt
        TopologyBuilder builder = new TopologyBuilder();

        // Only one source ("spout") for the words
        builder.setSpout(DOCUMENT_SPOUT, spout, 1);

        // Four parallel word counters, each of which gets specific words
        builder.setBolt(PROCESS_BOLT, bolt, NUM_PRROCESSING_BOLTS).shuffleGrouping(DOCUMENT_SPOUT);
        // Four parallel word counters, each of which gets specific words
        builder.setBolt(ACC_BOLT, acc_bolt, NUM_COLLECTOR_BOLTS).fieldsGrouping(PROCESS_BOLT, new Fields("word"));

        // A single printer bolt (and officially we round-robin)
        //builder.setBolt(PRINT_BOLT, printer, 4).shuffleGrouping(COUNT_BOLT);

        cluster = new LocalCluster();
        Topology topo = builder.createTopology();

        ObjectMapper mapper = new ObjectMapper();
        try {
            String str = mapper.writeValueAsString(topo);

            System.out.println("The StormLite topology is:\n" + str);
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        cluster.submitTopology("Indexer", config,
                builder.createTopology());
        Thread.sleep(MAX_RUN_TIME*1000);
        shutdown();
    }


}
