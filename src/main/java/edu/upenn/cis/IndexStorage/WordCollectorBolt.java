package edu.upenn.cis.IndexStorage;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.model.DocumentEntity;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class WordCollectorBolt implements IRichBolt {

    private static String indexDB_location = "store/indexDB";
    HashMap<String, TfIdfEntity> entities;

    static final String executorID = UUID.randomUUID().toString();

    static private int clusterID = 0;


    private Map<String, String> stormConf;
    private OutputCollector collector;
    private TopologyContext context;

    private Fields schema = new Fields("word", "url","tf","is_title_tf");
    private Fields schema_out = new Fields();

    @Override
    public void cleanup() {
        TfIdfDatabaseContainer db = TfIdfDatabaseContainer.getInstance(indexDB_location);
        int size = entities.keySet().size();
        LoadingGUI loader = new LoadingGUI("Bolt #"+clusterID+" uploading", size, 2000);
        for (String word : entities.keySet()) {
            loader.load();
            db.addTfIdf(entities.get(word));
        }
        clusterID--;
        if (clusterID == 0) db.close();
    }

    @Override
    public boolean execute(Tuple input) {
        String word = (String) input.getObjectByField("word");
        String url = (String) input.getObjectByField("url");
        Double freq = (Double) input.getObjectByField("tf");
        boolean is_title_freq = (boolean) input.getObjectByField("is_title_tf");
        store_freq(word, url, freq, is_title_freq);
        return true;
    }

    private void store_freq(String word, String url, Double tf, boolean is_title_freq) {
        if (entities.containsKey(word)) {
            if (is_title_freq) {
                entities.get(word).set_ttf(url, tf);
            } else {
                entities.get(word).set_tf(url, tf);
            }
        } else {
            TfIdfEntity e = new TfIdfEntity(word);
            if (is_title_freq) {
                e.set_ttf(url, tf);
            } else {
                e.set_tf(url, tf);
            }
            entities.put(word, e);
        }
    }

    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.stormConf = stormConf;
        this.collector = collector;
        this.context = context;
        //db = TfIdfDatabaseContainer.getInstance(indexDB_location);
        entities = new HashMap<String, TfIdfEntity>();
        clusterID++;
    }

    @Override
    public void setRouter(StreamRouter router) {
        this.collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return schema_out;
    }

    @Override
    public String getExecutorId() {
        return executorID;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(schema_out);
    }
}
