package edu.upenn.cis.stormlite.bolt;

import edu.upenn.cis.stormlite.StatCollector;
import edu.upenn.cis.stormlite.spout.CrawlerSpout;
import edu.upenn.cis455.mapreduce.model.DocumentEntity;
import edu.upenn.cis455.mapreduce.storage.DatabaseContainer;
import edu.upenn.cis455.mapreduce.utils.info.URLInfo;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.util.BlockingArrayQueue;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.print.Doc;
import java.util.*;
import java.util.concurrent.BlockingQueue;

public class DatabaseBolt implements IRichBolt {

    OutputCollector collector;
    Map<String, String> stormConf;
    TopologyContext context;

    Fields schema = new Fields("document");
    String executorId = UUID.randomUUID().toString();
    Logger logger = LogManager.getLogger(DatabaseBolt.class);
    BlockingQueue<DocumentEntity> queue;
    BlockingQueue<DocumentEntity> urlsToAdd;

    static boolean sentEOF = false;

    int workerNumber = 0;

    @Override
    public void cleanup() {
        sentEOF = false;
       // db.printContents();
        new WriterThread(urlsToAdd, DatabaseContainer.getInstance()).start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // logger.debug("Closing DB");
    }

    @Override
    public boolean execute(Tuple input) {
        if (sentEOF) {
            DatabaseContainer db = DatabaseContainer.getInstance();
            DocumentEntity doc = (DocumentEntity) input.getObjectByField("document");
            // logger.debug("Database Bolt executing " + doc.getUrl());
            try {

                if (db.containsDocument(doc.getUrl())) {
                    logger.debug(doc.getUrl() + " already in DB");
                    return true;
                }

                //logger.debug(doc.toString());
                if (doc == null) return true;

                // lump db writes together
                urlsToAdd.add(doc);
                StatCollector.getInstance(workerNumber).incrementURLsIndexed();
                //logger.debug(urlsToAdd.size());
                synchronized (urlsToAdd) {
                    if (urlsToAdd.size() > 100) {
                        new WriterThread(urlsToAdd, db).start();
                        urlsToAdd.clear();
                    }
                }
            } catch (IllegalStateException e) {
                // when shutting down
            }

            return true;
        }
        // stream has been shutdown - no more work to do
        return false;
    }

    // thread writes docs to DB in batches of 100
    private class WriterThread extends Thread {
        private BlockingQueue<DocumentEntity> docs;
        private DatabaseContainer db;

        public WriterThread(BlockingQueue<DocumentEntity> docs, DatabaseContainer db) {

            this.docs = copyDocs(docs);
            this.db = db;
        }

        private synchronized BlockingQueue<DocumentEntity> copyDocs(BlockingQueue<DocumentEntity> docs) {
            BlockingQueue<DocumentEntity> copy = new BlockingArrayQueue<>();
            for (DocumentEntity doc : docs) {
                copy.add(doc);
            }
            return copy;
        }

        @Override
        public void run() {
            System.out.println("Writing "  + docs.size() +  " Documents to DB!");
            synchronized (db) {
                    db.addDocuments(docs);
            }
        }
    }

    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.stormConf = stormConf;
        this.collector = collector;
        this.context = context;

        String storagePath = stormConf.get("storage");
        workerNumber = Integer.valueOf(stormConf.get("workerIndex")) + 1;
        sentEOF = true;

        queue = CrawlerSpout.getQueueInstance(storagePath);
        urlsToAdd = new BlockingArrayQueue<>();
    }

    @Override
    public void setRouter(StreamRouter router) {
        this.collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return schema;
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(schema);
    }


}
