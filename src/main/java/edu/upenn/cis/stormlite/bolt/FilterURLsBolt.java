package edu.upenn.cis.stormlite.bolt;

import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.mapreduce.model.DocumentEntity;
import edu.upenn.cis455.mapreduce.utils.info.RobotsTxtInfo;
import edu.upenn.cis455.mapreduce.utils.info.RobotsTxtParser;
import edu.upenn.cis455.mapreduce.utils.info.URLInfo;
import edu.upenn.cis455.mapreduce.storage.DatabaseContainer;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.spout.CrawlerSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

import static java.lang.Thread.sleep;

public class FilterURLsBolt implements IRichBolt {
    static final String USER_AGENT = "cis455crawler";
    OutputCollector collector;
    Map<String, String> stormConf;
    TopologyContext context;

    Fields schema = new Fields("document");
    String executorId = UUID.randomUUID().toString();
    Logger logger = LogManager.getLogger(FilterURLsBolt.class);

    String path;

    static boolean sentEOF  = false;

    static int maxSize;
    static int maxFileCount;
    static int currentCrawlCount;

    @Override
    public void cleanup() {
        // todo
        sentEOF = false;
    }

    // method receives a list of urls from crawlbolt and filters them based on robots.txt and size constraints
    @Override
    public boolean execute(Tuple input) {
        if (sentEOF) {
            DocumentEntity doc = (DocumentEntity) input.getObjectByField("document");
            try {
                //if (currentCrawlCount > maxFileCount) return;
                //  logger.debug("Executing filterurls bolt for url: " + doc.getUrl());
                String type = doc.getContentType();
                if ((type.contains("text/xml") || type.contains("text/html") || type.contains("application/xml") || type.contains("htm")
                        || type.endsWith("+xml"))) {
                    //logger.debug("processing out links");
                    //logger.debug(doc.getContent());
                    processOutGoingLinks(doc);

                }

                // add new urls to spout
                for (String url : doc.getOutgoingURLs()) {
                    DocumentEntity newDoc = new DocumentEntity();
                    // remove query params
                    String noQueryURL = url;
                    // logger.debug(noQueryURL);
                    if (noQueryURL.contains("?")) {
                        noQueryURL = url.substring(0, url.indexOf("?"));
                    }

                    if (noQueryURL.contains("#")) {
                        // fragment - link to same page
                        //https://www.oho.com/blog/explained-60-seconds-hash-symbols-urls-and-seo
                        continue;
                    }

                    // links get p trashy
                    if (noQueryURL.contains("twitter") || noQueryURL.contains("youtube") ||
                            noQueryURL.contains("instagram") || noQueryURL.contains("t.co") ||
                            noQueryURL.contains("facebook") || noQueryURL.contains("amazon")) {
                        continue;
                    }

                    if (noQueryURL.contains(".svg") || noQueryURL.contains(".jpg") || noQueryURL.contains("pdf")) {
                        continue;
                    }


                    newDoc.setUrl(noQueryURL);
                    CrawlerSpout.addToQueue(newDoc, path);


                    // prevent queue from ballooning out of control
                /*
                if (CrawlerSpout.getQueueInstance(path).size() < 1000) {
                    CrawlerSpout.addToQueue(newDoc, path);
                }
                */
                }

                // send doc to be written to DB
                collector.emit(new Values<Object>(doc), executorId);

            } catch (Exception e) {
                // e.printStackTrace();
                collector.emit(new Values<Object>(doc), executorId);
            }
            return true;
        }
        return false;
    }

    private void processOutGoingLinks(DocumentEntity doc) {
        // String hostname = new URLInfo(doc.getUrl()).getHostName();
        //Document document = Jsoup.parse(doc.getContent());
        Document document = null;
        try {
            document = Jsoup.connect(doc.getUrl()).get();
        } catch (IOException e) {
            //e.printStackTrace();
            return;
        }

        Elements linksOnPage = document.select("a[href]");
        List<String> urls = new ArrayList<>();
        for (Element link : linksOnPage) {
                String absUrl = link.attr("abs:href");
                if (absUrl != null) {
                    //logger.debug(absUrl);
                    urls.add(absUrl);
                }
        }

        doc.addOutgoingURLs(urls);
    }


    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.stormConf = stormConf;
        this.collector = collector;
        this.context = context;

        this.path = stormConf.get("storage");
        sentEOF = true;

    }

    @Override
    public void setRouter(StreamRouter router) {
        collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return schema;
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(schema);
    }


}
