package edu.upenn.cis.stormlite.bolt;

import edu.upenn.cis.stormlite.StatCollector;
import edu.upenn.cis.stormlite.spout.CrawlerSpout;
import edu.upenn.cis455.mapreduce.model.DocumentEntity;
import edu.upenn.cis455.mapreduce.utils.info.RobotsTxtInfo;
import edu.upenn.cis455.mapreduce.utils.info.RobotsTxtParser;
import edu.upenn.cis455.mapreduce.utils.info.URLInfo;
//import edu.upenn.cis455.mapreduce
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

import edu.upenn.cis.stormlite.tuple.Values;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.util.*;

public class CrawlBolt implements IRichBolt {

    Logger logger = LogManager.getLogger(CrawlBolt.class);
    static final String USER_AGENT = "cis455crawler";
    static final String executorID = UUID.randomUUID().toString();

    static Integer maxSize = 1000000;

    private Map<String, String> stormConf;
    private OutputCollector collector;
    private TopologyContext context;
    int workerNumber = 0;
    static boolean sentEOF = true;

    private Fields schema = new Fields("document");

    String path;

    @Override
    public void cleanup() {
        sentEOF = false;
    }

    @Override
    public boolean execute(Tuple input) {
        if (sentEOF) {
            // get url from input
            DocumentEntity doc = (DocumentEntity) input.getObjectByField("document");
            //  logger.debug("Crawl Bolt executing url: " + doc.getUrl());

            StatCollector.getInstance(workerNumber).incrementURLsCrawled();


            URLInfo info = new URLInfo(doc.getUrl());

            String protocol = info.isSecure() ? "https://" : "http://";
            RobotsTxtParser robots = new RobotsTxtParser(protocol + info.getHostName() + "/robots.txt");
            robots.parse();

            String lastTimeCrawled = doc.getLastCrawledTime();

            if (!robotsAllowed(robots, info.getFilePath())) {
                return false;
            }

            // Send Head Requests to check if OK to crawl
            try {
                if (doc.getUrl().startsWith("http://")) {
                    Socket socket = new Socket(info.getHostName(), info.getPortNo());
                    PrintWriter pw = new PrintWriter(socket.getOutputStream());
                    sendRequest(pw, info, "HEAD", lastTimeCrawled);
                    //  logger.debug("head request sent");

                    InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    String firstLine = bufferedReader.readLine();

                    // check if document has been modified since last crawl time
                    // no need to parse again
                    if (firstLine.trim().contains("304")) {
                        logger.debug("Not Modified");
                        collector.emit(new Values<Object>(doc), executorID);
                        pw.close();
                        socket.close();
                        bufferedReader.close();
                        return true;
                    }

                    // logger.debug("before parse headers");

                    // get headers from response
                    Map<String, String> responseHeaders = parseHeaders(bufferedReader);
                    //  logger.debug("parse headers read");

                    // check MIME type
                    if (!validContentType(responseHeaders)) {
                        logger.debug("incompatible mime type");
                        pw.close();
                        socket.close();
                        bufferedReader.close();
                        return true;
                    }

                    doc.setContentType(responseHeaders.get("content-type"));
                    pw.close();
                    socket.close();
                    bufferedReader.close();
                    // logger.debug("http head request sent");
                } else if (doc.getUrl().startsWith("https://")) {
                    URL https = new URL(doc.getUrl());
                    HttpsURLConnection connection = (HttpsURLConnection) https.openConnection();
                    connection.setConnectTimeout(3000);
                    connection.setReadTimeout(2000);
                    connection.setRequestProperty("User-Agent", USER_AGENT);

                    if (doc.getLastCrawledTime() != null) {
                        connection.setRequestProperty("If-Modified-Since", doc.getLastCrawledTime());
                    }

                    connection.setRequestMethod("HEAD");
                    int responseCode = 0;

                    try {
                        responseCode = connection.getResponseCode();
                    } catch (Exception e) {
                        return false;
                        //e.printStackTrace();
                    }
                    if (responseCode == 304) {
                        // not modified since
                        logger.debug("not modified since");
                        collector.emit(new Values<Object>(doc), executorID);
                        connection.disconnect();
                        return true;
                    } else if (responseCode == 429) {
                        // too many concurrent requests
                        logger.debug("concurrent requests to url: " + doc.getUrl());
                        // CrawlerSpout.shuffleQueue();
                        return true;
                    } else if (responseCode != 200) {
                        logger.debug("response code is not valid");
                        return true;
                    }

                    String size = connection.getHeaderField("Content-Length");

                    // make sure file size isnt too big
                    if (size != null) {
                        try {
                            int contentSize = Integer.parseInt(size);
                            if (contentSize > maxSize) {
                                logger.debug("over max size");
                                connection.disconnect();
                            }
                        } catch (NumberFormatException nfe) {
                            logger.debug("nfe at " + doc.getUrl());
                        }
                    }


                    // check mime
                    String mimeType = connection.getHeaderField("Content-Type");
                    if (mimeType == null) {
                        connection.disconnect();
                        return true;

                    }

                    // dont parse these files
                    if (mimeType.contains("svg")
                            || mimeType.contains("jpg") || mimeType.contains("image")) {
                        // logger.debug("incompatible mime type");
                        connection.disconnect();
                        return true;
                    }

                    // parse these
                    if (!mimeType.contains("text/html") && !mimeType.contains("text/xml")
                            && !mimeType.contains("application/xml") && !mimeType.contains("+xml")) {
                        //   logger.debug("incompatible mime type");
                        connection.disconnect();
                        return true;

                    }
                    doc.setContentType(mimeType);
                }
            } catch (Exception e) {
                logger.debug("Exception thrown reading " + doc.getUrl());
                // e.printStackTrace();
                return false;
            }


            // now make a GET request to get page content
            try {
                if (doc.getUrl().startsWith("http://")) {
                    //   Socket socket = new Socket(info.getHostName(), info.getPortNo());
                    //  PrintWriter pw = new PrintWriter(socket.getOutputStream());
                    //  sendRequest(pw, info, "GET", doc.getLastCrawledTime());

                /*
                InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
                //while (!inputStreamReader.ready());

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line = null;
                // skip response headers;
                while ((line = bufferedReader.readLine()) != null && line.length() != 0);

                StringBuilder sb = new StringBuilder();
                logger.debug("Reading page at " + doc.getUrl());

                // read content
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line);
                }

                */
                    Document jsoupDoc = Jsoup.connect(doc.getUrl()).get();
                    doc.setContent(jsoupDoc.html());

                    // doc.setContent(sb.toString());

                    // send doc to next bolt
                    collector.emit(new Values<>(doc), executorID);
                    //   socket.close();
                    // bufferedReader.close();
                    // inputStreamReader.close();
                    //  logger.debug("http get request sent");

                } else if (doc.getUrl().startsWith("https://")) {
                /*
                HttpsURLConnection connection = (HttpsURLConnection) new URL(doc.getUrl()).openConnection();
                connection.setConnectTimeout(3000);
                connection.setReadTimeout(2000);
                connection.setRequestProperty("User-Agent", USER_AGENT);
                connection.setRequestMethod("GET");

*/
                /*
                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line);
                }

                 */

                    // switched to jsoup for performance
                    Document jsoupDoc = Jsoup.connect(doc.getUrl()).get();
                    doc.setContent(jsoupDoc.html());
                    //doc.setContent(sb.toString());
                    collector.emit(new Values<>(doc), executorID);
                    //bufferedReader.close();
                    // connection.disconnect();
                }

            } catch (Exception e) {
                // e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    // for http req
    private boolean validContentType(Map<String, String> responseHeaders) {
        if (responseHeaders.containsKey("content-type")) {
            logger.debug(responseHeaders.get("content-type"));
        }
        return !responseHeaders.containsKey("content-type") || (responseHeaders.containsKey("content-type")
                && (!responseHeaders.get("content-type").contains("text/html")
                && !responseHeaders.get("content-type").contains("text/xml")
                && !responseHeaders.get("content-type").contains("application/xml")
                && !responseHeaders.get("content-type").contains("+xml") &&
                responseHeaders.get("content-type").contains("image")));
    }

    // helper method for requests
    private void sendRequest(PrintWriter pw, URLInfo info, String type, String lastTimeCrawled) {
        pw.write(type + " " + info.getFilePath() + " HTTP/1.1\r\n");
        pw.write("Host: " + info.getHostName() + "\r\n");
        if (lastTimeCrawled != null) {
            pw.write("If-Modified-Since: " + lastTimeCrawled + "\r\n");
        }

        pw.write("Connection: close\r\n");
        pw.write("User-Agent: " + USER_AGENT + "\r\n");
        pw.write("\r\n");
        pw.flush();
    }

    // parse throu
    private Map<String, String> parseHeaders(BufferedReader br) {
        Map<String, String> headers = new HashMap<String, String>();
        try {
            String line = br.readLine();

            while (line != null) {
                if (line.indexOf(':') != -1) {
                    String[] header = line.split(":", 2);
                    String value = header[1].trim();
                    String key = header[0].trim().toLowerCase();

                    if (key.equals("content-type") && value.indexOf(";") != -1)
                        value = value.substring(0, value.indexOf(";"));


                    headers.put(key, value);

                }
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return headers;
    }

    // robots.txt parsing
    private boolean robotsAllowed(RobotsTxtParser robots, String site) {
        RobotsTxtInfo defaultInfo = robots.getInfo();

        // check wild card user agent and "cis455crawler"
        boolean OKtoCrawl = true;

        ArrayList<String> allowedLinks = new ArrayList<>();
        ArrayList<String> disallowedLinks = new ArrayList<>();

        if (defaultInfo.containsUserAgent("USER_AGENT")) {
            if (defaultInfo.getAllowedLinks(USER_AGENT) != null) {
                allowedLinks = defaultInfo.getAllowedLinks(USER_AGENT);
            }

            if (defaultInfo.getDisallowedLinks(USER_AGENT) != null) {
                disallowedLinks = defaultInfo.getDisallowedLinks(USER_AGENT);
            }
        } else if (defaultInfo.containsUserAgent("*")) {
            if (defaultInfo.getAllowedLinks("*") != null) {
                allowedLinks = defaultInfo.getAllowedLinks("*");
            }

            if (defaultInfo.getDisallowedLinks("*") != null) {
                disallowedLinks = defaultInfo.getDisallowedLinks("*");
            }
        }

        for (String link : disallowedLinks) {
            if (site.contains(link) || site.equals(link)) {
                OKtoCrawl = false;
            }
        }

        // if allowed link is present
        for (String link : allowedLinks) {
            if (link.equals(site)) {
                OKtoCrawl = true;
            }
        }

        int delay = 0;

        if (defaultInfo.crawlContainAgent(USER_AGENT)) {
            delay = defaultInfo.getCrawlDelay(USER_AGENT);
        } else if (defaultInfo.crawlContainAgent("*")) {
            delay = defaultInfo.getCrawlDelay("*");
        }

        // if crawl delay, wait specified number of seconds before continuing
        if (delay > 0) deferCrawl(delay);

        return OKtoCrawl;
    }

    public void deferCrawl(int length) {
        try {
            logger.debug("Waiting to Crawl");
            Thread.sleep(length * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private boolean canParse(String url) {
        return false;
    }


    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.stormConf = stormConf;
        this.collector = collector;
        this.context = context;

        this.path = stormConf.get("storage");
        workerNumber = Integer.valueOf(stormConf.get("workerIndex")) + 1;
        sentEOF = true;
    }

    @Override
    public void setRouter(StreamRouter router) {
        this.collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return schema;
    }

    @Override
    public String getExecutorId() {
        return executorID;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(schema);
    }


}
