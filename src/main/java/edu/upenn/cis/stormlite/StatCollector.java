package edu.upenn.cis.stormlite;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StatCollector {
    private static StatCollector instance;
    private int workerNumber = 0;

    private int URLsCrawled;
    private int URLsIndexed;

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private StatCollector(int workerNumber) {
        this.workerNumber = workerNumber;
    }


    public static StatCollector getInstance(int workerNumber) {
        if (instance == null) {
            instance = new StatCollector(workerNumber);
        }
        return instance;
    }


    public void incrementURLsCrawled() {
        URLsCrawled++;
        if (URLsCrawled % 50 == 0) {
            System.out.println(getNumberCrawled());
        }
    }

    public String getNumberCrawled() {
        return ("Worker #" + workerNumber + " has crawled " + URLsCrawled + " web pages at time: " +
                dateFormat.format(new Date()));
    }

    public void incrementURLsIndexed() {
        URLsIndexed++;
        if (URLsIndexed % 10 == 0) {
            System.out.println(getNumberIndexed());
        }
    }

    public String getNumberIndexed() {
        return ("Worker #" + workerNumber + " has indexed " + URLsIndexed + " web pages at time: " +
                dateFormat.format(new Date()));
    }


}
