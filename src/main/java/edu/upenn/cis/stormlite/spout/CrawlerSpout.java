package edu.upenn.cis.stormlite.spout;

import edu.upenn.cis455.mapreduce.model.DocumentEntity;
import edu.upenn.cis455.mapreduce.utils.info.URLInfo;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.util.BlockingArrayQueue;

import javax.print.Doc;
import java.io.*;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class CrawlerSpout implements IRichSpout {
    Logger logger = LogManager.getLogger(CrawlerSpout.class);
    Map<String, String> config;
    TopologyContext topo;
    SpoutOutputCollector collector;
    String path;

    static BlockingQueue<DocumentEntity> queue;
    static Set<DocumentEntity> seenURLs;

    String executorId = UUID.randomUUID().toString();

    // initalize URL queue
    public static BlockingQueue<DocumentEntity> getQueueInstance(String path) {
        if (queue == null) {
            queue = new BlockingArrayQueue<>();
          // DocumentEntity doc = new DocumentEntity();
           //doc.setUrl("https://dbappserv.cis.upenn.edu/crawltest.html");
          //  doc.setUrl("https://stackoverflow.com/questions/9886531/how-to-parse-xml-with-jsoup");
          // queue.add(doc);

         queue.addAll(loadSeedURLs(path + "/savedQueue.txt"));
         if (queue.isEmpty()) {
             queue.addAll(loadSeedURLs(path + "/seeds.txt"));
         }

        }
        return queue;
    }

    public static void shuffleQueue(String path) {
       //Collections.shuffle(queue);
    }



    public static List<DocumentEntity> loadSeedURLs(String path) {
        File file = new File(path);
        List<DocumentEntity> docs = new ArrayList<>();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                DocumentEntity doc = new DocumentEntity();
                doc.setUrl(line.trim());
                docs.add(doc);
                seenURLs.add(doc);
            }

            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("seed urls added from " + path);
        return docs;
    }

    @Override
    public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
        this.config = config;
        this.topo = topo;
        this.collector = collector;

        this.path = config.get("storage");
        seenURLs = new HashSet<>();
       // System.out.println(path);

    }

    @Override
    public void close() {

    }

    @Override
    public synchronized boolean nextTuple() {
        if (!getQueueInstance(path).isEmpty()) {

            DocumentEntity doc = getQueueInstance(path).remove();
          //  System.out.println("Spout outputting url " + doc.getUrl() + " in nextTuple() from " + path);
            collector.emit(new Values(doc), executorId);
        }
       // System.out.println("No URL in Queue");
        return true;
    }

    public static synchronized void addToQueue(DocumentEntity doc, String path) {
        if (seenURLs.contains(doc)) {
            return;
        }
        // prevent excess of URLs and duplicates
        if (getQueueInstance(path).size() < 1000) {
            getQueueInstance(path).add(doc);
            seenURLs.add(doc);
        }
    }

    @Override
    public void setRouter(StreamRouter router) {
        collector.setRouter(router);
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("document"));
    }

    public static void writeQueueToFile(String path) {
        System.out.println("Crawler is writing queue to file.");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(path + "/savedQueue.txt", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        for (DocumentEntity doc : queue) {
            writer.println(doc.getUrl());
        }
        writer.close();

    }

}
